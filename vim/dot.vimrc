set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
Plugin 'majutsushi/tagbar'
Plugin 'craigemery/vim-autotag'
Plugin 'Valloric/YouCompleteMe'
Plugin 'vim-syntastic/syntastic'
Plugin 'airblade/vim-gitgutter'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'nvie/vim-flake8'
Plugin 'lepture/vim-jinja'
Plugin 'lervag/vimtex'
Plugin 'vim-airline/vim-airline'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'hdima/python-syntax'
Plugin 'xuhdev/vim-latex-live-preview'

Plugin 'arcticicestudio/nord-vim'
Plugin 'morhetz/gruvbox'
Plugin 'tpozzi/Sidonia'
Plugin 'tomasr/molokai'
Plugin 'dracula/vim'

" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin Configurations

" nerdtree
nmap <F8> :NERDTreeToggle<CR>

" tagbar
set tags=./tags;,tags;$HOME
nmap <F9> :TagbarToggle<CR>
nmap <F10> :TagbarOpen<Space>j<CR>
let g:tagbar_autofocus = 1
let g:tagbar_left = 1

" vim-autotag
let g:autotagTagsFile="tags"

" YouCompleteMe
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
let g:ycm_global_ycm_extra_conf = '/home/yescalona/.ycm_extra_conf.py' 

" syntastic
let g:syntastic_python_python_exec = 'python3'
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++ -Wall -Wextra -Wpedantic'
let g:syntastic_cpp_check_header = 1
let g:syntastic_python_checkers=['flake8']
let g:syntastic_python_flake8_args='--max-line-length=120'
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

" vim-gitguttter
let g:gitgutter_max_signs=1000

" vim-latex-live-preview
let g:livepreview_cursorhold_recompile = 0

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General Configurations
" colorscheme
let python_highlight_all=1
syntax on
set background=dark
if has('gui_running')
    "let g:gruvbox_contrast_dark='dark'
    colorscheme dracula
else
    colorscheme dracula
endif

" font
set encoding=utf-8
set guifont=Ubuntu\ Mono\ 16
"set guifont=Noto\ Mono\ Regular\ 16
"set guifont=Input\ Mono\ Narrow\ Regular\ 15

" by default in other configurations
set hlsearch " highlight all search matches
set number " display line numbers
set backspace=2 " make backspace work like most other apps
set backspace=indent,eol,start " recommended by vim 

" indent 
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

autocmd FileType html setlocal ts=2 sts=2 sw=2
autocmd FileType css setlocal ts=2 sts=2 sw=2
autocmd FileType javascript setlocal ts=2 sts=2 sw=2
autocmd FileType c setlocal ts=2 sts=2 sw=2
autocmd FileType cpp setlocal ts=2 sts=2 sw=2


"set guioptions-=m
"set guioptions-=T


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Special Configurations

" NAMD files
au BufNewFile,BufRead *.inp  set filetype=tcsh
au BufNewFile,BufRead *.in   set filetype=tcsh
au BufNewFile,BufRead *.namd set filetype=tcsh
au BufNewFile,BufRead *.colvars set filetype=tcsh

" GROMOS files
au BufNewFile,BufRead *.arg set filetype=tcsh
au BufNewFile,BufRead *.mtb set filetype=perl
au BufNewFile,BufRead *.imd set filetype=maple
au BufNewFile,BufRead *.top set filetype=perl

au BufNewFile,BufRead *.tex 
            \ set guifont=Ubuntu\ Mono\ 20 |
            \ set linebreak |
            \ setlocal spell spelllang=en_us 

autocmd FileType tex
            \ setlocal ts=2 sts=2 sw=2 |
            \ nmap <F9> :VimtexTocOpen<CR> |
            \ set linespace=4
